﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace testProject1.Pages.Locators
{
    public class HomePageLocators
    {
        public By AvatarIcon
        {
            get { return By.CssSelector(".user-avatar > img"); }
        }
    }
}
