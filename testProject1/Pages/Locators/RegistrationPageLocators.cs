﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace testProject1.Pages.Locators
{
    public class RegistrationPageLocators
    {
        public By SignUpFormModal
        {
            get { return By.ClassName("modal-content"); }
        }

        public By EmailInputSignUp
        {
            get { return By.XPath(".//input[@name='email']"); }
        }

        public By PasswordInputSignUp
        {
            get { return By.XPath(".//input[@name='password']"); }
        }

        public By ConfirmPasswordInputSignUp
        {
            get { return By.XPath(".//input[@name='confirmPassword']"); }
        }

        public By UsernameInputSignUp
        {
            get { return By.XPath(".//input[@name='username']"); }
        }

        public By SignUpButtonInModal
        {
            get { return By.XPath(".//button[@class='btn pmc-btn-primary btn-modal width-120']"); }
        }
    }
}
