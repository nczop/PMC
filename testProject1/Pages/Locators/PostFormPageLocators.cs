﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using testProject1.Pages.Executors;

namespace testProject1.Pages.Locators
{
    public class PostFormPageLocators
    {
        public By SelectCategoriesInPostFrom
        {
            get { return By.CssSelector(".custom-radio"); }
        }        

        public By TitleInput
        {
            get { return By.CssSelector("input[name='title']"); }
        }

        public By ContentInput
        {
            get { return By.CssSelector(".notranslate.public-DraftEditor-content"); }
        }

        public By LoginAndAddButton
        {
            get { return By.CssSelector(".btn.pmc-btn-warning.width-180.align-items-center.margin-top-minus-74"); }
        }

        public By Category(string categoryName)
        {
            return By.CssSelector(string.Format(".//label[text()='{0}']/..", categoryName));
        }

    }
}
