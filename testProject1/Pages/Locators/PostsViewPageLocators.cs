﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace testProject1.Pages.Locators
{
    public class PostsViewPageLocators
    {
        public By Posts
        {
            get { return By.CssSelector(".post-container"); }
        }       
    }
}
