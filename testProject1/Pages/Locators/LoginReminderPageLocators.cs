﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace testProject1.Pages.Locators
{
    public class LoginReminderPageLocators
    {
        public By LoginButton
        {
            get { return By.XPath("//*[text()='Log in']"); }
        }

        public By SignUpButton
        {
            get { return By.XPath(".//button[text()='Sign up']"); }
        }
    }
}
