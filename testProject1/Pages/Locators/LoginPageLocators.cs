﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace testProject1.Pages.Locators
{
    public class LoginPageLocators
    {
        public By EmailInput
        {
            get { return By.CssSelector("input[name='email']"); }
        }

        public By PasswordInput
        {
            get { return By.CssSelector("input[name='password']"); }
        }

        public By SumbitButton
        {
            get { return By.CssSelector("button[type='submit']"); }
        }

        public By ValidationMessage
        {
            get { return By.Id("Message"); }
        }
    }
}
