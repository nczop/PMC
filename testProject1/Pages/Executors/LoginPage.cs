﻿using testProject1.Models.Objects;
using testProject1.Pages.Locators;

namespace testProject1.Pages.Executors
{
    public class LoginPage : LoginPageLocators
    {
        public void Login(User user)
        { 
            this.EnterEmail(user.Email);
            this.EnterPassword(user.Password);
            this.SubmitLoginForm();
        }             

        public void EnterEmail(string email)
        {
            Wait.UntiElementVisible(this.EmailInput);
            this.EmailInput.GetElement().SendKeys(email);
        }

        public void EnterPassword(string password)
        {
            Wait.UntiElementVisible(this.PasswordInput);
            this.PasswordInput.GetElement().SendKeys(password);
        }

        public void SubmitLoginForm()
        {
            Wait.UntiElementVisible(this.SumbitButton);
            this.SumbitButton.GetElement().Click();
        }
    }
}
