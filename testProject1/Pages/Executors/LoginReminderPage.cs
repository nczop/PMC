﻿
using testProject1.Pages.Locators;

namespace testProject1.Pages.Executors
{
    public class LoginReminderPage : LoginReminderPageLocators
    {
        public void OpenLoginForm()
        {
            Wait.UntiElementVisible(this.LoginButton);
            this.LoginButton.GetElement().Click();
        }

        public void OpenRegistrationForm()
        {
            Wait.UntiElementVisible(this.SignUpButton);
            this.SignUpButton.GetElement().Click();
        }
    }
}
