﻿using System;
using System.ComponentModel;
using testProject1.Models.Objects;
using testProject1.Pages.Locators;
using testProject1.Utilities;
using testProject1.Utilities.Enums;

namespace testProject1.Pages.Executors
{
    public class PostFormPage : PostFormPageLocators
    {


        public void AddPost(Post post)
        {
            this.SelectCategory(post.Category);
            this.EnterTitle(post.Title);
            this.EnterContent(post.Content);
            this.SubmitPostForm();
        }

        public void EnterPost(Post addPostForm)
        {
            this.EnterTitle(addPostForm.Title);
            this.EnterContent(addPostForm.Content);
        }

        public void EnterTitle(string title)
        {
            Wait.UntiElementVisible(this.TitleInput);
            this.TitleInput.GetElement().SendKeys(title);
        }

        public void EnterContent(string content)
        {
            Wait.UntiElementVisible(this.ContentInput);
            this.ContentInput.GetElement().SendKeys(content);
        }

        public void SubmitPostForm()
        {
            Wait.UntiElementVisible(this.LoginAndAddButton);
            this.LoginAndAddButton.GetElement().Click();
        }

        public void SelectCategory(Category category)
        {
            var categoryName = category.GetEnumDescription();
            this.Category(categoryName).GetElement().Click();
        }
    }
}

