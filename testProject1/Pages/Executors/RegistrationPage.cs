﻿using testProject1.Models.Objects;
using testProject1.Pages.Locators;

namespace testProject1.Pages.Executors
{
    public class RegistrationPage : RegistrationPageLocators
    {        
        public void NewAccout (Registration registrationForm)
        {
            this.EnterEmailSignUp(registrationForm.Email);
            this.EnterPasswordSignUp(registrationForm.Password);
            this.EnterConfirmPasswordSignUp(registrationForm.ConfirmPassword);
            this.EnterUsernameSignUp(registrationForm.Username);
        }

        public void EnterEmailSignUp (string emailSignUp)
        {
            this.EmailInputSignUp.SendKeys(emailSignUp);
        }

        public void EnterPasswordSignUp (string passwordSignUp)
        {
            this.PasswordInputSignUp.SendKeys(passwordSignUp);
        }

        public void EnterConfirmPasswordSignUp (string confirmPasswordSignUp)
        {
            this.ConfirmPasswordInputSignUp.SendKeys(confirmPasswordSignUp);
        }

        public void EnterUsernameSignUp (string usernameSignUp)
        {
            this.UsernameInputSignUp.SendKeys(usernameSignUp);
        }

    }
}
