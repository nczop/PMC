﻿using testProject1.Pages.Locators;

namespace testProject1.Pages.Executors
{
    public class CategoryPage : CategoryPageLocators
    {
        public void SelectCategory(int categoryNumber)
        {
            var indexOfCategory = categoryNumber - 1;

            Wait.UntiElementVisible(this.CategorySection);
            this.CategorySection.GetElements()[indexOfCategory].Click();
        }
        
    }
}
