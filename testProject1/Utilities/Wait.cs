﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;

namespace testProject1
{
    public static class Wait
    {
        public static void UntiElementVisible(By elementLocator)
        {
            SeleniumExecutor.GetWaitDriver().Until(ExpectedConditions.ElementIsVisible(elementLocator));
        }

    }
}
