﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace testProject1.Utilities
{
    public static class EnumExtensions
    {

        public static string GetEnumDescription(this Enum enumInput)
        {
            var type = enumInput.GetType();

            var memInfo = type.GetMember(enumInput.ToString());
            if (memInfo != null && memInfo.Length > 0)
            {
                object[] description = memInfo[0].GetCustomAttributes(typeof(DescriptionAttribute), false);

                if (description != null && description.Length > 0)
                {
                    return ((DescriptionAttribute)description[0]).Description;
                }
            }

            return enumInput.ToString();
        }
    }
}
