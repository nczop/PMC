﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace testProject1.Utilities.Enums
{
    public enum Category
    {
        [Description("Frameworks")]
        Frameworks,

        [Description("Programming languagues")]
        ProgrammingLanguagues,

        [Description("Databases")]
        Databases,

        [Description("Design patterns")]
        DesignPatterns,

        [Description("DevOps")]
        DevOps,

        [Description("Mobile")]
        Mobile,

        [Description("Operating systems")]
        OperatingSystems,

        [Description("Security")]
        Security,

        [Description("UX")]
        UX,

        [Description("Web")]
        Web
    }
}
