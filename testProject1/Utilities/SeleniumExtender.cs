﻿using OpenQA.Selenium;
using System.Collections.Generic;

namespace testProject1
{
    public static class SeleniumExtender
    {
        public static bool IsDisplayed(this By elementLocator)
        {
            try
            {
                var element = SeleniumExecutor.Driver.FindElement(elementLocator);
                return element.Displayed;
            }
            catch (NoSuchElementException)
            {
                return false;
            } 
        }

        public static IWebElement GetElement(this By elementLocator)
        {
            return SeleniumExecutor.Driver.FindElement(elementLocator);
        }

        public static IList<IWebElement> GetElements(this By elementLocator)
        {
            return SeleniumExecutor.Driver.FindElements(elementLocator);
        }

        public static void SendKeys(this By elementLocator, string text)
        {
            elementLocator.GetElement().SendKeys(text);
        }
    }  
}
