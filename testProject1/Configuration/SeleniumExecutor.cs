﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Support.UI;
using System;

namespace testProject1
{
    public static class SeleniumExecutor
    {
        private static IWebDriver webDriver;

        public static IWebDriver Driver
        {
            get { return webDriver ?? (webDriver = Initialize()); }
        }

        public static IWebDriver Initialize()
        {
            var options = new ChromeOptions();
            options.AddArgument("--start-maximized");
            return new ChromeDriver(options);
        }

        public static WebDriverWait GetWaitDriver()
        {
            return new WebDriverWait(Driver, TimeSpan.FromSeconds(20));
        }

        public static void Quit()
        {
            Driver.Quit();
        }
    }
}
