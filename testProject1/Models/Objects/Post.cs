﻿
using testProject1.Utilities.Enums;

namespace testProject1.Models.Objects
{
    public class Post
    {
        public Category Category { get; set; }

        public string Title { get; set; }

        public string Content { get; set; }

        

        public Post (Category category, string title, string content)
        {
            this.Category = category;
            this.Title = title;
            this.Content = content;

        }
                
    }
}
