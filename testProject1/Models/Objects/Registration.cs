﻿
namespace testProject1.Models.Objects
{
    public class Registration
    {
        public Registration(string email, string password, string confirmPassword, string username)
        {
            this.Email = email;
            this.Password = password;
            this.ConfirmPassword = confirmPassword;
            this.Username = username;
        }

        public string Email { get; set; }

        public string Password { get; set; }

        public string ConfirmPassword { get; set; }

        public string Username { get; set;  }
    }
}
