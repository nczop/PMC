﻿
namespace testProject1.Models.Objects
{
    public class User
    {
        public User(string email, string password)
        {
            this.Email = email;
            this.Password = password;
        }

        public string Email { get; set; }

        public string Password { get; set; }
    }
}
