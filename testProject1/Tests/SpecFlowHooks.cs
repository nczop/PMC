﻿using TechTalk.SpecFlow;

namespace testProject1.Tests
{
    [Binding]
    public class SpecFlowHooks
    {
        [AfterScenario]
        public static void AfterScenario()
        {
            ScenarioContext.Current.Clear();
            SeleniumExecutor.Driver.Manage().Cookies.DeleteAllCookies();
        }

        [AfterTestRun]
        public static void AfterTestRun()
        {
            SeleniumExecutor.Quit();
        }
    }
}
