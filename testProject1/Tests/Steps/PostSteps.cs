﻿using NUnit.Framework;
using TechTalk.SpecFlow;
using testProject1.Models.Objects;
using testProject1.Pages.Executors;
using testProject1.Utilities.Enums;

namespace testProject1.Tests.Steps
{
    [Binding]
    public class PostSteps
    {
        PostFormPage newPostFormPage = new PostFormPage();
        LoginPage loginPage = new LoginPage();
        
        [Given(@"I am on the add post view page")]
        public void GivenIAmOnTheAddPostViewPage()
        {
            SeleniumExecutor.Driver.Url = "https://pimpmycode.azurewebsites.net/posts/add";
        }
        
        [When(@"I select the category")]
        public void WhenISelectTheCategory()
        {
            this.newPostFormPage.SelectCategory(Category.OperatingSystems);
        }
        
        [When(@"I enter the date into the form")]
        public void WhenIEnterTheDateIntoTheForm()
        {
            var post = new Post(Category.DevOps, "Test", "TekstContentTekstContentTekstContent");
            this.newPostFormPage.AddPost(post);
        }
        
        [When(@"I submit add post form")]
        public void WhenISubmitAddPostForm()
        {
            this.newPostFormPage.SubmitPostForm();
        }
        
        [Then(@"Log in form is displayed")]
        public void ThenLogInFormIsDisplayed()
        {
            Wait.UntiElementVisible(this.loginPage.SumbitButton);
            Assert.True(this.loginPage.SumbitButton.IsDisplayed());
        }
    }
}
