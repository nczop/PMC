﻿using NUnit.Framework;
using System;
using TechTalk.SpecFlow;
using testProject1.Pages.Executors;

namespace testProject1.Tests.Steps
{
    [Binding]
    public class RegistrationSteps
    {
        public LoginReminderPage loginReminderPage = new LoginReminderPage();
        public RegistrationPage registrationPage = new RegistrationPage();

        [When(@"I open registration form")]
        public void WhenIOpenRegistrationForm()
        {
            this.loginReminderPage.OpenRegistrationForm();           
        }
        
        [Then(@"Registration modal is displayed")]
        public void ThenRegistrationModalIsDisplayed()
        {
            Wait.UntiElementVisible(this.registrationPage.SignUpFormModal);
            Assert.True(this.registrationPage.SignUpFormModal.IsDisplayed());
        }


        

        [When(@"I enter the correct date")]
        public void WhenIEnterTheCorrectDate()
        {
            //var newAccount = new RegistrationForm("pmctestpgs+a@gmail.com", "Test1234!", "Test1234!", "testUsername");
        }

        [When(@"I submit the registration form")]
        public void WhenISubmitTheRegistrationForm()
        {
            ScenarioContext.Current.Pending();
        }

        [Then(@"The sign up toast is displayed")]
        public void ThenTheSignUpToastIsDisplayed()
        {
            ScenarioContext.Current.Pending();
        }





    }

}
