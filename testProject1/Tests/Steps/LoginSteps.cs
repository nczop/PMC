﻿using NUnit.Framework;
using System;
using TechTalk.SpecFlow;
using testProject1.Models.Objects;
using testProject1.Pages.Executors;
using testProject1.Utilities.Resources;

namespace testProject1.Tests.Steps
{
    [Binding]
    public class LoginSteps 
    {
        LoginPage loginPage = new LoginPage();
        LoginReminderPage loginReminderPage = new LoginReminderPage();

        [Given(@"I am on the home page")]
        public void GivenIAmOnTheLoginPage()
        {
            SeleniumExecutor.Driver.Url = "https://pimpmycode.azurewebsites.net";
        }
               
        [When(@"I log in using valid credentials")]
        public void WhenILogIn()
        {
            
            var user = new User("pmctestpgs@gmail.com", "Test1234!");
            this.loginPage.Login(user);
        }
        
        [Then(@"Avatar is displayed")]
        public void ThenAvatarIsDisplayed()
        {
            var homePage = new HomePage();
            Wait.UntiElementVisible(homePage.AvatarIcon);            
            Assert.True(homePage.AvatarIcon.IsDisplayed());
        }

        [Given(@"I open log in form")]
        public void GivenIOpenLogInForm()
        {
            this.loginReminderPage.OpenLoginForm();
        }

        [When(@"I enter invalid credentials")]
        public void WhenIEnterInvalidCredentials()
        {
            var user = new User("test@", "test");
            this.loginPage.EnterEmail(user.Email);
            this.loginPage.EnterPassword(user.Password);
        }

        [When(@"I submit the form")]
        public void WhenISubmitTheForm()
        {
            this.loginPage.SubmitLoginForm();
        }

        [Then(@"I can see  validation message")]
        public void ThenICanSeeValidationMessage()
        {
            Assert.AreEqual(Messages.LoginValidationMessage, this.loginPage.ValidationMessage.GetElement().Text);
        }

    }
}
