﻿using NUnit.Framework;
using System;
using System.Linq;
using TechTalk.SpecFlow;
using testProject1.Pages.Executors;

namespace testProject1.Tests.Steps
{
    [Binding]
    public class CategorySteps
    {

        CategoryPage CategoryPage = new CategoryPage();
        PostViewPage postViewPage = new PostViewPage();

        [When(@"I select the category from the list")]
        public void WhenISelectTheCategoryFromTheList()
        {

            this.CategoryPage.SelectCategory(2);
        }
        
        [Then(@"List of post is displayed")]
        public void ListOfPostIsDisplayed()
        {
            Assert.True(postViewPage.Posts.GetElements().All(a => a.Displayed));
        }
    }
}
