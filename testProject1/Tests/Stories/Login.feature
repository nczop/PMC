﻿Feature: Login

Scenario: User can login using valid credentials
	Given I am on the home page	
	And I open log in form
	When I log in using valid credentials
	Then Avatar is displayed

Scenario: Error message is displayed when trying to login using invalid credentials
	Given I am on the home page 
	And I open log in form 
	When I enter invalid credentials 
	And I submit the form
	Then I can see  validation message 


