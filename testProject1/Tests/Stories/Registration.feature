﻿Feature: Registration
	
Scenario: After click on Sign up button, registration modal appears
	Given I am on the home page
	When  I open registration form
	Then Registration modal is displayed

Scenario: User want to create a new account 
	Given  I am on the home page 
	When I open registration form 
	And I enter the correct date
	And I submit the registration form
	Then The sign up toast is displayed